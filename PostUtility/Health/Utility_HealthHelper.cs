﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;
using PostUtility.Model;

namespace PostUtility.Health
{
    public static class Utility_HealthHelper
    {
        private static Dictionary<string, string> response { get; set; }
        public static Dictionary<string, string> GetSumInsuredPriceList()
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "tbl_SumInsuredMaster");
            response.Add("where", string.Empty);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetNewEnquiryHealthDetails(string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "tbl_SumInsuredMaster");
            response.Add("where", "Enquiry_Id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetEnquiry_Response(string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_Enquiry_Response");
            response.Add("where", "enquiry_id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetNewEnquiry_Health(string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_NewEnquiry_Health");
            response.Add("where", "Enquiry_Id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetMaster_PinCode(string pincode)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "top 1 City");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "tblMaster_PinCode");
            response.Add("where", "Pincode=" + pincode);
            response.Add("database", "motor");
            return response;
        }
        public static Dictionary<string, string> InsertSelectedPlanProposal(string enquiryid, string productid, string companyid, string suminsured, string periodfor, string premium, string discount, string policyfor, string plantype)
        {
            string fileds = "enquiryid,productid,companyid,suminsured,periodfor,premium,discount,policyfor,plantype";
            string fieldValue = "'" + enquiryid + "','" + productid + "','" + companyid + "','" + suminsured + "','" + periodfor + "','" + premium + "','" + discount + "','" + policyfor + "','" + plantype + "'";

            response = new Dictionary<string, string>();
            response.Add("type", "insert");
            response.Add("column", fileds);
            response.Add("fieldvalue", fieldValue);
            response.Add("table", "T_Health_Proposal");
            response.Add("where", string.Empty);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateSelectedPlanProposal(string suminsured, string periodfor, string premium, string discount, string policyfor, string plantype, string enquiryid, string productid, string companyid)
        {
            string fieldsWithValue = "suminsured='" + suminsured + "',periodfor='" + periodfor + "',premium='" + premium + "',discount='" + discount + "',policyfor='" + policyfor + "',plantype='" + plantype + "'";
            string whereCondition = "enquiryid='" + enquiryid + "' and productid='" + productid + "' and companyid='" + companyid + "'";

            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", fieldsWithValue);
            response.Add("table", "T_Health_Proposal");
            response.Add("where", whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> InsertProposerDetailsProposal(string dob, string address1, string address2, string landmark, int stateid, string statename, int cityid, string cityname, string firstname, string lastname, string mobile, string emailid, string pincode, string gender, bool isproposerinsured, string title, string panno, string aadharno, string annualincome, bool ispropnominee)
        {
            string fileds = "dob,address1,address2,landmark,stateid,statename,cityid,cityname,firstname,lastname,mobile,emailid,pincode,gender,isproposerinsured,title,panno,aadharno,annualincome,ispropnominee";
            string fieldValue = "'" + dob + "','" + address1 + "','" + address2 + "','" + landmark + "','" + stateid + "','" + statename + "','" + cityid + "','" + cityname + "','" + firstname + "','" + lastname + "','" + mobile + "','" + emailid + "','" + pincode + "','" + gender + "'," + isproposerinsured + ",'" + title + "','" + panno + "','" + aadharno + "','" + annualincome + "'," + ispropnominee + "";

            response = new Dictionary<string, string>();
            response.Add("type", "insert");
            response.Add("column", fileds);
            response.Add("fieldvalue", fieldValue);
            response.Add("table", "T_Health_Proposal");
            response.Add("where", string.Empty);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateProposerDetailsProposal(string dob, string address1, string address2, string landmark, int stateid, string statename, int cityid, string cityname, string title, string firstname, string lastname, string mobile, string emailid, string pincode, string gender, int areaid, string areaname, string panno, string aadharno, string annualincome, string enquiryid, string productid, string companyid, bool criticalillness, bool isproposerinsured, bool ispropnominee)
        {
            string fieldsWithValue = "dob='" + dob + "',address1='" + address1 + "',address2='" + address2 + "',landmark='" + landmark + "',statename='" + statename + "',title='" + title + "',cityid='" + cityid + "',cityname='" + cityname + "',firstname='" + firstname + "',lastname='" + lastname + "',mobile='" + mobile + "',emailid='" + emailid + "',pincode='" + pincode + "',gender='" + gender + "',areaid='" + areaid + "',areaname='" + areaname + "',criticalillness=" + (criticalillness ? 1 : 0) + ",isproposerinsured=" + (isproposerinsured ? 1 : 0) + ",ispropnominee=" + (ispropnominee ? 1 : 0) + ",panno='" + panno + "',aadharno='" + aadharno + "',annualincome='" + annualincome + "'";
            string whereCondition = "enquiryid='" + enquiryid + "' and productid='" + productid + "' and companyid='" + companyid + "'";

            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", fieldsWithValue);
            response.Add("table", "T_Health_Proposal");
            response.Add("where", whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> IsEnquiryDetailExistOrNot(string enquiryid, string productid, string companyid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_Health_Proposal");
            response.Add("where", "enquiryid='" + enquiryid + "' and productid='" + productid + "' and companyid='" + companyid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetProposalDetail(string enquiryid, string productid = "", string companyid = "")
        {
            string wherecon = "enquiryid='" + enquiryid + "'";

            if (!string.IsNullOrEmpty(productid.Trim()))
            {
                wherecon = wherecon + "  and productid='" + productid + "'";
            }
            if (!string.IsNullOrEmpty(companyid.Trim()))
            {
                wherecon = wherecon + "  and companyid='" + companyid + "'";
            }

            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_Health_Proposal");
            response.Add("where", wherecon);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetRelation(string companyid, string productId, string policyfor)
        {
            string wherecon = "CompanyId='" + companyid + "' and status=1";
            if (companyid == "3")
            {
                wherecon = wherecon + " and RelationType like '%" + policyfor + "%'";
            }

            if (companyid == "1")
            {
                wherecon = wherecon + " and productdetailid='" + productId + "'";
            }

            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_Relationship_Common");
            response.Add("where", wherecon);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateSearchUrlPara_InEnquiry_Response(string enquiryid, string urlpara)
        {
            string[] paraSplit = urlpara.Split('&');

            string respoFieldsWithValue = "productid='" + paraSplit[1].Split('=')[1] + "',suminsuredid='" + paraSplit[3].Split('=')[1] + "',suminsured='" + paraSplit[4].Split('=')[1] + "',companyid='" + paraSplit[2].Split('=')[1] + "'";

            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", respoFieldsWithValue);
            response.Add("table", "T_Enquiry_Response");
            response.Add("where", "enquiry_id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateSearchUrlPara_InNewEnquiry_Health(string enquiryid, string urlpara, string selectedproductjson)
        {
            string fieldsWithValue = "searchurl='" + urlpara + "',selectedproductjson='" + selectedproductjson + "'";
            string whereCondition = "Enquiry_Id='" + enquiryid + "'";

            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", fieldsWithValue);
            response.Add("table", "T_NewEnquiry_Health");
            response.Add("where", whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> IsInsuredDetailExistOrNot(string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_Health_Insured");
            response.Add("where", "enquiryid='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> InsertInsuredDetails(string enquiryid, string productid, string companyid, int relationid, string relationname, string title, string firstname, string lastname, string dob, int height, int weight, bool isinsured, string issame, string occupationid, string isengagemanuallabour, string isengagewintersports, string isillness, string illnessdesc, string engageManualLabourDesc, string engageWinterSportDesc)
        {
            string fileds = "enquiryid,productid,companyid,relationid,relationname,title,firstname,lastname,dob,height,weight,isinsured,issame,occupationId,engageManualLabour,engageWinterSports,illness,illnessdesc,engageManualLabourDesc,engageWinterSportDesc";
            string fieldValue = "'" + enquiryid + "','" + productid + "','" + companyid + "','" + relationid + "','" + relationname + "','" + title + "','" + firstname + "','" + lastname + "','" + dob + "','" + height + "','" + weight + "'," + (isinsured ? "1" : "0") + "," + (isinsured ? "1" : "0") + ",'" + occupationid + "'," + (Convert.ToBoolean(isengagemanuallabour.ToLower()) ? "1" : "0") + "," + (Convert.ToBoolean(isengagewintersports.ToLower()) ? "1" : "0") + ", " + (Convert.ToBoolean(isillness.ToLower()) ? "1" : "0") + ",'" + illnessdesc + "','" + engageManualLabourDesc + "','" + engageWinterSportDesc + "'";

            response = new Dictionary<string, string>();
            response.Add("type", "insert");
            response.Add("column", fileds);
            response.Add("fieldvalue", fieldValue);
            response.Add("table", "T_Health_Insured");
            response.Add("where", string.Empty);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateInsuredDetails(string enquiryid, string productid, string companyid, int relationid, string relationname, string title, string firstname, string lastname, string dob, int height, int weight, bool isinsured, string issame, string occupationid, string isengagemanuallabour, string isengagewintersports, string isillness, string illnessdesc, string engageManualLabourDesc, string engageWinterSportDesc, int id)
        {
            string fieldsWithValues = "enquiryid='" + enquiryid + "',productid='" + productid + "',companyid='" + companyid + "',relationid='" + relationid + "',relationname='" + relationname + "',title='" + title + "',firstname='" + firstname + "',lastname='" + lastname + "',dob='" + dob + "',height='" + height + "',weight='" + weight + "',isinsured=" + (isinsured ? "1" : "0") + ",occupationId='" + occupationid + "',engageManualLabour=" + (Convert.ToBoolean(isengagemanuallabour.ToLower()) ? "1" : "0") + ",engageWinterSports=" + (Convert.ToBoolean(isengagewintersports.ToLower()) ? "1" : "0") + ",illness=" + (Convert.ToBoolean(isillness.ToLower()) ? "1" : "0") + ",illnessdesc='" + illnessdesc + "',engageManualLabourDesc='" + engageManualLabourDesc + "',engageWinterSportDesc='" + engageWinterSportDesc + "'";

            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", fieldsWithValues);
            response.Add("table", "T_Health_Insured");
            response.Add("where", "enquiryid='" + enquiryid + "' and id=" + id + "");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateNomineeDetail(string title, string firstname, string lastname, string dob, int age, string relation, string enquiryid)
        {
            string fieldsWithValue = "ntitle='" + title + "',nfirstname='" + firstname + "',nlastname='" + lastname + "',ndob='" + dob + "',nage='" + age + "',nrelation='" + relation + "'";
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", fieldsWithValue);
            response.Add("table", "T_Health_Proposal");
            response.Add("where", "enquiryid='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetInsuredDetails(string enquiryid, string productid = "", string companyid = "")
        {
            string wherecon = "enquiryid='" + enquiryid + "'";
            if (!string.IsNullOrEmpty(productid.Trim()))
            {
                wherecon = wherecon + " and productid='" + productid + "'";
            }
            if (!string.IsNullOrEmpty(companyid.Trim()))
            {
                wherecon = wherecon + " and companyid='" + companyid + "'";
            }
            wherecon = wherecon + " order by id";

            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_Health_Insured");
            response.Add("where", wherecon);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateQuestionerDetails(string questionjson, string enquiryid, string firstname = "", string lastnane = "", string id = "")
        {
            string fieldsWithValue = "medicalhistory='" + questionjson.Replace("'", "''") + "'";
            string whereCondition = "enquiryid='" + enquiryid + "'";
            if (!string.IsNullOrEmpty(firstname.Trim()))
            {
                whereCondition = whereCondition + " and firstname='" + firstname + "'";
            }
            if (!string.IsNullOrEmpty(lastnane.Trim()))
            {
                whereCondition = whereCondition + " and lastname='" + lastnane + "'";
            }
            if (!string.IsNullOrEmpty(id.Trim()))
            {
                whereCondition = whereCondition + " and id='" + id + "'";
            }

            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", fieldsWithValue);
            response.Add("table", "T_Health_Insured");
            response.Add("where", whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetPaymentDetail(string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_Health_Payment");
            response.Add("where", "enquiryid='" + enquiryid + "' order by id");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> InsertPaymentDetail(string enquiry_id, string referenceId, string premium, string totalPremium, string proposalNum, string productDetailId, string companyId, string paymenturl, string servicetax)
        {
            string fileds = "enquiryId,pp_referenceId,company,pp_companyId,pp_premium,pp_totalPremium,pp_proposalNum,pp_productDetailId,pp_rowpaymenturl,logoUrl,pp_serviceTax";
            string fieldValue = "'" + enquiry_id + "','" + referenceId + "',(select CompanyName from [seeinsuredapi_test].[dbo].[tbl_Company] where CompanyId=" + companyId + "),'" + companyId + "','" + premium + "','" + totalPremium + "','" + proposalNum + "','" + productDetailId + "','" + paymenturl + "',(select logoUrl from [seeinsuredapi_test].[dbo].[tbl_Company] where CompanyId=" + companyId + "),'" + servicetax + "'";

            response = new Dictionary<string, string>();
            response.Add("type", "insert");
            response.Add("column", fileds);
            response.Add("fieldvalue", fieldValue);
            response.Add("table", "T_Health_Payment");
            response.Add("where", string.Empty);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdatePaymentDetail(string enquiry_id, string referenceId, string premium, string totalPremium, string proposalNum, string productDetailId, string companyId, string paymenturl, string servicetax)
        {
            string fieldsWithValue = "pp_referenceId='" + referenceId + "',pp_premium='" + premium + "',pp_totalPremium='" + totalPremium + "',pp_proposalNum='" + proposalNum + "',pp_productDetailId='" + productDetailId + "',pp_companyId='" + companyId + "',pp_rowpaymenturl='" + paymenturl + "',pp_serviceTax='" + servicetax + "',pay_proposal_Number='" + proposalNum + "'";
            string whereCondition = "enquiryid='" + enquiry_id + "'";

            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", fieldsWithValue);
            response.Add("table", "T_Health_Payment");
            response.Add("where", whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateProposalPaymentDetail(string p_proposalNum, string p_totalPremium, string companyId, string p_rowpaymenturl, string enquiryid, string p_referenceId)
        {
            string p_fieldsWithValue = "p_proposalNum='" + p_proposalNum + "',p_totalPremium='" + p_totalPremium + "',logoUrl=(select logoUrl from [seeinsuredapi_test].[dbo].[tbl_Company] where CompanyId=" + companyId + "),p_rowpaymenturl='" + p_rowpaymenturl + "'";
            if (companyId == "2")
            {
                p_fieldsWithValue = p_fieldsWithValue + ",p_referenceId='" + p_proposalNum + "',pay_proposal_Number='" + p_proposalNum + "'";
            }
            else
            {
                p_fieldsWithValue = p_fieldsWithValue + ",p_referenceId = '" + p_referenceId + "'";
            }
            string p_whereCondition = "enquiryid='" + enquiryid + "'";

            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", p_fieldsWithValue);
            response.Add("table", "T_Health_Proposal");
            response.Add("where", p_whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdatePaymentDetail(string p_proposalNum, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "pp_referenceId='" + p_proposalNum + "'");
            response.Add("table", "T_Health_Payment");
            response.Add("where", "enquiryid='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetOccupationDetail(string compantId)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "tbl_OccupationMaster");
            response.Add("where", "CompanyID = '" + compantId + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetNominee_CommonDetails(string companyid, string type)
        {
            string wherecon = "CompanyId=" + companyid + " and Type='" + type + "' order by Relationship";
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "tbl_Nominee_Common");
            response.Add("where", wherecon);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetAnnualIncomeDetails(string enquiryid, string companyid = "")
        {
            string wherecon = "";
            if (!string.IsNullOrEmpty(companyid))
            {
                wherecon = "CompanyId='" + companyid + "'";
            }
            else
            {
                wherecon = "CompanyId in (select companyid from T_Health_Proposal where enquiryid='" + enquiryid + "')";
            }

            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "Master_Annual_Income");
            response.Add("where", wherecon);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateHealthPolicyPdfLink(string enquiryid, string policypdflink, string websiteUrl)
        {
            string fieldsWithValue = "policypdflink='" + (websiteUrl + policypdflink) + "'";
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", fieldsWithValue);
            response.Add("table", "T_Health_Proposal");
            response.Add("where", "enquiryid='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }

        public static Dictionary<string, string> UpdateCarePayment_HealthProposal(string enquiryid, string policyNumber)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "pay_policy_Number='" + policyNumber + "'");
            response.Add("table", "T_Health_Proposal");
            response.Add("where", "enquiryid='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdatePaymentDate_HealthProposal(string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "pay_paymentdate=getdate()");
            response.Add("table", "T_Health_Proposal");
            response.Add("where", "enquiryid='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateCarePaymentDetails(string policyNumber, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string status, string enquiryid, string purchaseToken = "")
        {
            string fieldsWithValue = "cc_policyNumber='" + policyNumber + "',cc_transactionRefNum='" + transactionRefNum + "',cc_uwDecision='" + uwDecision + "',cc_errorFlag='" + errorFlag + "',cc_errorMsg='" + errorMsg + "',Star_PurchaseToken='" + purchaseToken + "',Status='" + status + "',payment_status='1',UpdatedDate=getdate()";

            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", fieldsWithValue);
            response.Add("table", "T_Health_Payment");
            response.Add("where", "enquiryid='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }

        public static Dictionary<string, string> UpdateRowRequest_EnquiryResponse(string requestJson, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "perposalrequest='" + requestJson.Replace("'", "''") + "'");
            response.Add("table", "T_Enquiry_Response");
            response.Add("where", "enquiry_id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateResponse_EnquiryResponse(string perposalresponse, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "perposalresponse='" + perposalresponse.Replace("'", "''") + "'");
            response.Add("table", "T_Enquiry_Response");
            response.Add("where", "enquiry_id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateRaw_Paymenturl_EnquiryResponse(string raw_paymenturl, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "paymentrequest='" + raw_paymenturl + "'");
            response.Add("table", "T_Enquiry_Response");
            response.Add("where", "enquiry_id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdatePaymentResponse_EnquiryResponse(string payresponse, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "paymentresponse='" + payresponse + "'");
            response.Add("table", "T_Enquiry_Response");
            response.Add("where", "enquiry_id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }

        public static Dictionary<string, string> UpdateQuestionnaireRequest_EnquiryResponse(string questionaryrequest, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "questionaryrequest='" + questionaryrequest + "'");
            response.Add("table", "T_Enquiry_Response");
            response.Add("where", "enquiry_id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateQuestionnaireResponse_EnquiryResponse(string questionaryrespo, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "questionaryresponse='" + questionaryrespo.Replace("'", "''") + "'");
            response.Add("table", "T_Enquiry_Response");
            response.Add("where", "enquiry_id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }

        public static Dictionary<string, string> UpdateProposalAndPolicyRequset_EnquiryResponse(string papRequest, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "ProposalAndPolicyReq='" + papRequest + "'");
            response.Add("table", "T_Enquiry_Response");
            response.Add("where", "enquiry_id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateProposalAndPolicyResponse_EnquiryResponse(string papResponse, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "ProposalAndPolicyRespo='" + papResponse + "'");
            response.Add("table", "T_Enquiry_Response");
            response.Add("where", "enquiry_id='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateRowRequest_HealthProposal(string requestJson, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "proposalreq='" + requestJson.Replace("'", "''") + "'");
            response.Add("table", "T_Health_Proposal");
            response.Add("where", "enquiryid='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateResponse_HealthProposal(string perposalresponse, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "perposalrespo='" + perposalresponse.Replace("'", "''") + "'");
            response.Add("table", "T_Health_Proposal");
            response.Add("where", "enquiryid='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdatePaymentDetail_HealthProposal(string proposal_Number, string pay_policy_Number, string enquiryid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "pay_proposal_Number='" + proposal_Number + "', pay_policy_Number='" + pay_policy_Number + "'");
            response.Add("table", "T_Health_Proposal");
            response.Add("where", "enquiryid='" + enquiryid + "'");
            response.Add("database", "api");
            return response;
        }


    }
}
