﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace PostUtility.Utility
{
    public static class UtilityClass
    {
        public static string GenrateEnquiryId(int sizelimit, string charcombo)
        {
            DateTime dtCurr = DateTime.Now;
            string todatdate = dtCurr.Year.ToString() + dtCurr.Month.ToString() + dtCurr.Day.ToString();
            return GenrateRandomTransactionId(todatdate, sizelimit, charcombo);
        }
        public static string GetFullPathWithAppData(string path, int agencyID)
        {
            string fullPath = "/App_Data/";

            if (!string.IsNullOrEmpty(path))
            {
                fullPath = fullPath + path + "/" + agencyID + "/";
            }

            return fullPath.Replace("//", "/");
        }
        public static string GetFullPathStringWithAppData(string path, string folderName)
        {
            string fullPath = "/App_Data/";

            if (!string.IsNullOrEmpty(path))
            {
                fullPath = fullPath + path + "/" + folderName + "/";
            }

            return fullPath.Replace("//", "/");
        }
        public static string GetImagePath(string imageName, string folderName, int id)
        {
            return !string.IsNullOrEmpty(imageName) ? "/GetFile.ashx?path=" + folderName + "/" + id + "/" + imageName : string.Empty;
        }
        public static string GetImagePath(string imageName, string folderName, string innerfoldername)
        {
            return !string.IsNullOrEmpty(imageName) ? "/GetFile.ashx?path=" + folderName + "/" + innerfoldername + "/" + imageName : string.Empty;
        }
        public static string GenrateRandomTransactionId(string prefixString, int SizeLimit, string charCombo = null)
        {
            try
            {
                char[] chars = !string.IsNullOrEmpty(charCombo) ? charCombo.ToCharArray() : "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                byte[] data = new byte[SizeLimit];
                using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
                {
                    crypto.GetBytes(data);
                }

                StringBuilder result = new StringBuilder(SizeLimit);

                if (!string.IsNullOrWhiteSpace(prefixString))
                {
                    result.Append(prefixString);
                }

                foreach (byte b in data)
                {
                    result.Append(chars[b % (chars.Length)]);
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static string GetDescription(string description, int? length = null)
        {
            string publicDescription = string.Empty;

            if (!string.IsNullOrEmpty(description))
            {
                publicDescription = description;

                publicDescription = publicDescription.Replace("\r\n", "<br/>").Replace("\n", "<br/>").Replace("amp;", "  ");

                if (length != null && publicDescription.Length > length)
                {
                    publicDescription = publicDescription.Remove((int)length);

                    if (publicDescription.LastIndexOf(" ") > 0)
                    {
                        publicDescription = publicDescription.Remove(publicDescription.LastIndexOf(" "));
                    }
                }
            }

            return publicDescription;
        }
        public static string SetSpaceAfterLength(string description, int? length = null)
        {
            string publicDescription = string.Empty;

            if (!string.IsNullOrEmpty(description))
            {
                description = description.Replace("\r\n", "<br/>").Replace("\n", "<br/>").Replace("amp;", "  ");

                if (length != null && description.Length > length)
                {
                    int loopcount = 0;
                    foreach (char c in description)
                    {
                        loopcount = loopcount + 1;
                        if (loopcount >= length)
                        {
                            loopcount = 0;
                            publicDescription = publicDescription + c + " ";
                        }
                        else
                        {
                            publicDescription = publicDescription + c;
                        }
                    }
                }
            }

            return publicDescription;
        }
        public static string FormatURL(string value)
        {
            return value.Replace("//", "/").Replace(":/", "://").ToLower().Trim();
        }
        public static string ConvertDateToISOFormate(string date)
        {
            string rutDate = date;
            if (!string.IsNullOrEmpty(date))
            {
                string[] splitDate = date.Split('/');
                rutDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];

                DateTime dtCDate = Convert.ToDateTime(rutDate);
                rutDate = dtCDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.sssZ");
            }
            return rutDate;
        }
    }
}
