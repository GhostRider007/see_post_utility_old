﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostUtility.Model
{
    public class NewEnquiry_Health
    {
        public string AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string Policy_Type { get; set; }
        public string Policy_Type_Text { get; set; }
        public string Sum_Insured { get; set; }
        public bool Self { get; set; }
        public int SelfAge { get; set; }
        public bool Spouse { get; set; }
        public int SpouseAge { get; set; }
        public bool Son { get; set; }
        public int Son1Age { get; set; }
        public int Son2Age { get; set; }
        public int Son3Age { get; set; }
        public int Son4Age { get; set; }
        public bool Daughter { get; set; }
        public int Daughter1Age { get; set; }
        public int Daughter2Age { get; set; }
        public int Daughter3Age { get; set; }
        public int Daughter4Age { get; set; }
        public bool Father { get; set; }
        public int FatherAge { get; set; }
        public bool Mother { get; set; }
        public int MotherAge { get; set; }
        public string Gender { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email_Id { get; set; }
        public string Mobile_No { get; set; }
        public string Pin_Code { get; set; }
        public bool Is_Term_Accepted { get; set; }
        public string Enquiry_Id { get; set; }
        public string searchurl { get; set; }
        public string selectedproductjson { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
    public class CreateHealth_Proposal
    {
        public int id { get; set; }
        public string enquiryid { get; set; }
        public string productid { get; set; }
        public string companyid { get; set; }
        public string suminsuredid { get; set; }
        public string suminsured { get; set; }
        public string periodfor { get; set; }
        public string policyfor { get; set; }
        public string plantype { get; set; }
        public string premium { get; set; }
        public string title { get; set; }
        public string discount { get; set; }
        public string dob { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string landmark { get; set; }
        public int stateid { get; set; }
        public string statename { get; set; }
        public int cityid { get; set; }
        public string cityname { get; set; }
        public int areaid { get; set; }
        public string areaname { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string mobile { get; set; }
        public string emailid { get; set; }
        public string pincode { get; set; }
        public string gender { get; set; }
        public bool isproposerinsured { get; set; }
        public bool criticalillness { get; set; }
        public bool ispropnominee { get; set; }
        public string proposaljson { get; set; }
        public List<InsuredDetails> InsuredDetails { get; set; }
        public string nRelation { get; set; }
        public string nTitle { get; set; }
        public string nFirstName { get; set; }
        public string nLastName { get; set; }
        public string nDOB { get; set; }
        public int nAge { get; set; }
        public string panno { get; set; }
        public string aadharno { get; set; }
        public string annualincome { get; set; }
        public string p_referenceId { get; set; }
        public string p_proposalNum { get; set; }
        public string p_totalPremium { get; set; }
        public string p_paymenturl { get; set; }
        public string logoUrl { get; set; }
        public string p_rowpaymenturl { get; set; }
        public string policypdflink { get; set; }
        public string pay_proposal_Number { get; set; }
        public string pay_policy_Number { get; set; }

    }
    public class InsuredDetails
    {
        public int id { get; set; }
        public int relationid { get; set; }
        public string relationname { get; set; }
        public string title { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string dob { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public bool isinsured { get; set; }
        public string occupationid { get; set; }
        public string isillness { get; set; }
        public string illnessdesc { get; set; }
        public string isengagemanuallabour { get; set; }
        public string isengagewintersports { get; set; }
        public string engageManualLabourDesc { get; set; }
        public string engageWinterSportDesc { get; set; }
    }
    public class Health_Insured
    {
        public int id { get; set; }
        public string enquiryid { get; set; }
        public string productid { get; set; }
        public int companyid { get; set; }
        public int relationid { get; set; }
        public string relationname { get; set; }
        public string title { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string dob { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public bool isinsured { get; set; }
        public string illnessdesc { get; set; }
        public bool issame { get; set; }
        public string medicalhistory { get; set; }
        public string occupationId { get; set; }
        public bool engageManualLabour { get; set; }
        public bool engageWinterSports { get; set; }
        public bool illness { get; set; }
        public string engageManualLabourDesc { get; set; }
        public string engageWinterSportDesc { get; set; }
    }

    public class Datum
    {
        public object inquiry_id { get; set; }
        public string premium { get; set; }
        public string basePremium { get; set; }
        public string serviceTax { get; set; }
        public string totalPremium { get; set; }
        public double discountPercent { get; set; }
        public string logoUrl { get; set; }
        public int schemaId { get; set; }
        public string insuranceCompany { get; set; }
        public int insuranceCompanyId { get; set; }
        public string policyType { get; set; }
        public int suminsured { get; set; }
        public string plan { get; set; }
        public string ageBracket { get; set; }
        public int suminsuredId { get; set; }
        public Product product { get; set; }
        public List<ProductTerm> productTerms { get; set; }
        public string quoteId { get; set; }
        public int id { get; set; }
    }
    public class Product
    {
        public string policyTypeName { get; set; }
        public string policyName { get; set; }
        public int postalCode { get; set; }
        public int period { get; set; }
        public List<Insured> insureds { get; set; }
        public object productDetailId { get; set; }
    }
    public class Insured
    {
        public object dob { get; set; }
        public int sumInsuredId { get; set; }
        public object buyBackPED { get; set; }
    }
    public class ProductTerm
    {
        public string planName { get; set; }
        public int sumInsuredMin { get; set; }
        public int sumInsuredMax { get; set; }
        public int ageGroupMin { get; set; }
        public int ageGroupMax { get; set; }
        public string medical_Tests { get; set; }
        public bool isCritical_Ilness { get; set; }
    }
}
