﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostUtility.Model
{
    //--------------Care payment check----------------------
    public class CarePaymentStatusCheckResponseData
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class CarePaymentStatusCheckIntGetPolicyStatusIO
    {
        public string proposalNum { get; set; }
        public string policyNum { get; set; }
        public string policyCommencementDt { get; set; }
        public string policyMaturityDt { get; set; }
        public string policyStatus { get; set; }
        public int policyPremium { get; set; }
        public string parentAgentId { get; set; }
        public string policyCommencementTimestamp { get; set; }
        public List<object> errorLists { get; set; }
        public List<object> listErrorListList { get; set; }
    }

    public class CarePaymentStatusCheckResponse
    {
        public CarePaymentStatusCheckResponseData responseData { get; set; }
        public CarePaymentStatusCheckIntGetPolicyStatusIO intGetPolicyStatusIO { get; set; }
    }

    public class CarePaymentStatusCheck
    {
        public bool success { get; set; }
        public string message { get; set; }
        public CarePaymentStatusCheckResponse response { get; set; }
        public int error_code { get; set; }
    }
    //----------------Star Payment----------
    public class PAP_Data
    {
        public string proposal_Number { get; set; }
        public string policy_Number { get; set; }
    }

    public class PAP_Response
    {
        public PAP_Data data { get; set; }
        public object urls { get; set; }
        public object schemesMaster { get; set; }
    }

    public class ProposalAndPolicy
    {
        public bool success { get; set; }
        public string message { get; set; }
        public PAP_Response response { get; set; }
        public int error_code { get; set; }
    }
}
