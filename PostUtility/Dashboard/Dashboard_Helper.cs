﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostUtility.Dashboard
{
    public static class Dashboard_Helper
    {
        private static Dictionary<string, string> response { get; set; }
        public static Dictionary<string, string> Get_HeatlhPolicy(string agencyid)
        {
            response = new Dictionary<string, string>();

            string fileds = " Top 5  'Nothing' as Fuel,'Nothing' as VarientName,'Nothing' as BrandName,'Nothing' as vehicletype,'Nothing' as ModelName,'Nothing' as VechileRegNo,hEnq.Sum_Insured ,(select ProductName from tbl_Product where ProductId=hProp.productid) as Prodcutname,(select CompanyName from tbl_Company where CompanyId=hProp.companyid)as CompanyName,hProp.suminsured,hEnq.CreatedDate,Enquiry_Id,hProp.firstname,hProp.lastname,hProp.p_totalPremium,hEnq.Policy_Type,'Health' as Service, 'Policy' as ServiceType";
            string tablename = "T_NewEnquiry_Health hEnq  inner join T_Health_Insured i on hEnq.enquiry_id=i.enquiryid  left join T_Health_Proposal hProp on hEnq.enquiry_id = hProp.enquiryid left join T_Health_Payment p on hEnq.enquiry_id = p.enquiryid";
            string whereCondition = " hProp.p_proposalNum is NOT NULL and hProp.pay_policy_Number is NOT NULL and hProp.pay_policy_Number <>'' and hEnq.AgencyId='"+agencyid+"' and i.isinsured=1  order by p.UpdatedDate desc";

            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> Get_HeatlhProposal(string agencyid)
        {
            response = new Dictionary<string, string>();

            string fileds = "  Top 5 'Nothing' as Fuel,'Nothing' as VarientName,'Nothing' as BrandName,'Nothing' as vehicletype,'Nothing' as ModelName,'Nothing' as VechileRegNo,Sum_Insured, (select ProductName from tbl_Product where ProductId=hProp.productid) as Prodcutname, (select CompanyName from tbl_Company where CompanyId=hProp.companyid) as CompanyName, hProp.suminsured,hEnq.CreatedDate,Enquiry_Id,hProp.firstname,hProp.lastname,hProp.p_totalPremium,Policy_Type,'Health' as Service, 'Proposal' as ServiceType";
            string tablename = " T_NewEnquiry_Health hEnq  inner join T_Health_Insured i on hEnq.enquiry_id=i.enquiryid left join T_Health_Proposal hProp on hEnq.enquiry_id = hProp.enquiryid left join T_Health_Payment p on hEnq.enquiry_id = p.enquiryid";
            string whereCondition = "  hProp.pay_policy_Number is NULL  and hProp.p_proposalNum is NOT NULL or hProp.p_proposalNum <> '' and i.isinsured=1  and  hEnq.AgencyId='"+agencyid+"' order by  hProp.createddate desc ";

            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> Get_HeatlhLead(string agencyid)
        {
            response = new Dictionary<string, string>();

            string fileds = " Top 5 'Nothing' as Fuel,'Nothing' as VarientName,'Nothing' as BrandName,'Nothing' as vehicletype,'Nothing' as ModelName,'Nothing' as VechileRegNo,Sum_Insured ,'Search' as Prodcutname,(select CompanyName from tbl_Company where CompanyId=hProp.companyid)as CompanyName,hProp.suminsured,hEnq.CreatedDate,Enquiry_Id,firstname,lastname,p_totalPremium,Policy_Type,'Health' as Service, 'Lead' as ServiceType";
            string tablename = " T_NewEnquiry_Health hEnq left join T_Health_Proposal hProp on hEnq.enquiry_id = hProp.enquiryid";
            string whereCondition = " hProp.pay_proposal_Number is NULL and hProp.pay_policy_Number is NULL and  hEnq.AgencyId='" + agencyid + "' order by  hEnq.CreatedDate desc";

            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> Get_MotorPolicy(string agencyid)
        {
            response = new Dictionary<string, string>();

            string fileds = " Top 5 hEnq.Fuel,hEnq.VarientName,hEnq.BrandName,hEnq.ModelName,hEnq.VechileRegNo,hEnq.vehicletype ,'Nothing' as Sum_Insured,'Nothing' as Prodcutname,(select Supplier  from T_MotorCredential where id=res.chainid)as CompanyName,res.suminsured,hEnq.CreatedDate,hEnq.Enquiry_Id,hProp.firstname,hProp.lastname,'Nothing' as p_totalPremium,hEnq.policyholdertype as Policy_Type,'Motor' as Service, 'Policy' as ServiceType";
            string tablename = " T_NewEnquiry_Motor hEnq inner join T_Motor_Proposal hProp on hEnq.Enquiry_Id = hProp.enquiry_id  left join T_Motor_Enquiry_Response res on hEnq.Enquiry_Id=res.enquiry_id";
            string whereCondition = " res.paymentstatus is NOT NULL and res.policynumber is NOT NULL and hEnq.AgencyId='" + agencyid + "' order by  hEnq.Id desc";

            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "motor");
            return response;
        }
        public static Dictionary<string, string> Get_MotorProposal(string agencyid)
        {
            response = new Dictionary<string, string>();

            string fileds = " Top 5 hEnq.Fuel,hEnq.VarientName,hEnq.BrandName,hEnq.ModelName,hEnq.VechileRegNo,hEnq.vehicletype ,'Nothing' as Sum_Insured,'Nothing' as Prodcutname,(select Supplier  from T_MotorCredential where id=res.chainid)as CompanyName,res.suminsured,hEnq.CreatedDate,hEnq.Enquiry_Id,hProp.firstname,hProp.lastname,'Nothing' as p_totalPremium,hEnq.policyholdertype as Policy_Type,'Motor' as Service, 'Proposal' as ServiceType";
            string tablename = " T_NewEnquiry_Motor hEnq inner join T_Motor_Proposal hProp on hEnq.Enquiry_Id = hProp.enquiry_id  left join T_Motor_Enquiry_Response res on hEnq.Enquiry_Id=res.enquiry_id";
            string whereCondition = " res.proposalnumber is NOT NULL and res.policynumber is NULL and hEnq.AgencyId='" + agencyid + "' order by  hEnq.Id desc";

            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "motor");
            return response;
        }
        public static Dictionary<string, string> Get_MotorLead(string agencyid)
        {
            response = new Dictionary<string, string>();

            string fileds = " Top 5 hEnq.Fuel,hEnq.VarientName,hEnq.BrandName,hEnq.ModelName,hEnq.VechileRegNo,hEnq.vehicletype ,'Nothing' as Sum_Insured,'Nothing' as Prodcutname,(select Supplier  from T_MotorCredential where id=res.chainid)as CompanyName,res.suminsured,hEnq.CreatedDate,hEnq.Enquiry_Id,hProp.firstname,hProp.lastname,'Nothing' as p_totalPremium,hEnq.policyholdertype as Policy_Type,'Motor' as Service, 'Lead' as ServiceType";
            string tablename = " T_NewEnquiry_Motor hEnq inner join T_Motor_Proposal hProp on hEnq.Enquiry_Id = hProp.enquiry_id  left join T_Motor_Enquiry_Response res on hEnq.Enquiry_Id=res.enquiry_id";
            string whereCondition = " res.proposalnumber is NULL and res.policynumber is NULL and hEnq.AgencyId='" + agencyid + "' order by  hEnq.Id desc";

            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "motor");
            return response;
        }
        public static Dictionary<string, string> Get_HealthTotalPremium(string agencyid, string period, string duration)
        {
            response = new Dictionary<string, string>();

            string fileds = " SUM(CAST(b.premium AS DECIMAL(18,2))) as SUM";
            string tablename = " T_NewEnquiry_Health a inner Join T_Health_Proposal b on a.enquiry_id = b.enquiryid  inner join T_Health_Payment c on a.enquiry_id = c.enquiryid";
            string whereCondition = " b.pay_proposal_Number is not null and a.AgencyId='" + agencyid + "' and c.CreatedDate>(SELECT DATEADD(" + period + "," + duration + ",CAST(CAST(GETDATE() AS DATE) AS DATETIME)))";

            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> Get_MotorTotalPremium(string agencyid, string period, string duration)
        {
            response = new Dictionary<string, string>();

            string fileds = " SUM(CAST(a.suminsured AS DECIMAL(18,2))) as SUM";
            string tablename = " T_Motor_Enquiry_Response a inner join T_Motor_Proposal b on a.enquiry_id = b.Enquiry_Id inner join T_NewEnquiry_Motor c on a.enquiry_id = c.Enquiry_Id ";
            string whereCondition = " a.policynumber is not null and c.AgencyId='" + agencyid + "' and a.policynumber <> '' and b.createddate>(SELECT DATEADD(" + period + "," + duration + ",CAST(CAST(GETDATE() AS DATE) AS DATETIME)))";

            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "motor");
            return response;
        }
    }
}
