﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostUtility.Admin_Backend
{
    public static class AdminBackend_EnquiryHelper
    {
        private static Dictionary<string, string> response { get; set; }
        public static Dictionary<string, string> GetEnquiryDetailsMotor(string listType, string AgencyId = "", string FromDate = "", string ToDate = "", string area = "admin", bool isFilter = false)
        {
            string fileds = "a.BlockID as BlockId, a.Enquiry_Id as Enquiry_Id, a.AgencyId as AgencyID ,a.AgencyName as AgencyName, b.proposalnumber as ProposalNumber, b.policynumber as PolicyNumber,";
            fileds = fileds + " (select supplier from T_MotorCredential where id = b.chainid ) as CompanyName, CONCAT(c.title,' ',c.firstname,' ',c.lastname) as CustomerName,";
            fileds = fileds + " a.vehicletype as VehicleClass, CONCAT(a.BrandName,' ',a.BrandName,' ',a.VarientName) as VehicleName,";
            fileds = fileds + "JSON_VALUE(b.proposalresponse,'$.ProductName') as ProductName,a.insurancetype as Policy_Type, b.suminsured as PremiumAmount,";
            fileds = fileds + "c.createddate as CreatedDate_Proposal, c.createddate as CreatedDate_Policy, b.policypdflink as PolicyPDFLink ";

            string tablename = " T_NewEnquiry_Motor a inner Join T_Motor_Enquiry_Response b on a.enquiry_id = b.enquiry_id  inner Join T_Motor_Proposal c on a.enquiry_id = c.enquiry_id";
            string whereCondition = string.Empty;

            if (!string.IsNullOrEmpty(AgencyId))
            {
                whereCondition = whereCondition + "a.AgencyId = '" + AgencyId + "' and ";
            }

            if (listType.ToLower() == "proposal")
            {
                whereCondition = whereCondition + " (b.transactionnumber is  null and b.proposalnumber is not Null and b.paymentstatus is null)";

                if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && area == "admin" && isFilter == false)
                {
                    whereCondition = whereCondition + " and  c.createddate > (select CAST(CAST(GETDATE() AS DATE) AS DATETIME))";
                }
                else
                {
                    if (!string.IsNullOrEmpty(FromDate))
                    {
                        whereCondition = whereCondition + " and c.createddate >= '" + FromDate + "'";
                    }
                    if (!string.IsNullOrEmpty(ToDate))
                    {
                        whereCondition = whereCondition + " and c.createddate <= DATEADD(Day, 1, '" + ToDate + "')";
                    }
                }
                whereCondition = whereCondition + "order by c.createddate desc";
            }
            if (listType.ToLower() == "policy")
            {
                whereCondition = whereCondition + " (b.transactionnumber is not null and b.policynumber is not Null and b.paymentstatus='success')";

                if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && area == "admin" && isFilter == false)
                {
                    whereCondition = whereCondition + " and  c.createddate > (select CAST(CAST(GETDATE() AS DATE) AS DATETIME))";
                }
                else
                {
                    if (!string.IsNullOrEmpty(FromDate))
                    {
                        whereCondition = whereCondition + " and c.createddate >= '" + FromDate + "'";
                    }
                    if (!string.IsNullOrEmpty(ToDate))
                    {
                        whereCondition = whereCondition + " and c.createddate <= DATEADD(Day, 1, '" + ToDate + "')";
                    }
                }

                whereCondition = whereCondition + " order by c.createddate desc";
            }

            if (area == "agency" && string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate))
            {
                fileds = "top 20 " + fileds;
            }

            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "motor");
            return response;
        }

    }
}
