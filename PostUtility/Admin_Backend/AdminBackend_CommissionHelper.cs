﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostUtility.Admin_Backend
{
    public static class AdminBackend_CommissionHelper
    {
        private static Dictionary<string, string> response { get; set; }
        public static Dictionary<string, string> GetCommissionList(string commisionId = "", string status = "")
        {
            string whereCondition = string.Empty;

            if (!string.IsNullOrEmpty(status))
            {
                whereCondition = "Status=" + status;
            }

            if (!string.IsNullOrEmpty(commisionId))
            {
                whereCondition += !string.IsNullOrEmpty(whereCondition) ? " and CommisionId=" + commisionId : "CommisionId=" + commisionId;
            }

            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_Commision");
            response.Add("where", whereCondition);
            response.Add("database", "seeinsured");
            return response;
        }

        public static Dictionary<string, string> InsertCommissionDetail(string basicPer, string basicTDS, string incentivePer, string incentiveTDS)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "insert");
            response.Add("column", "BasicPer,BasicTDS,IncentivePer,IncentiveTDS");
            response.Add("fieldvalue", "'" + basicPer + "','" + basicTDS + "','" + incentivePer + "','" + incentiveTDS + "'");
            response.Add("table", "T_Commision");
            response.Add("where", string.Empty);
            response.Add("database", "seeinsured");
            return response;
        }
        public static Dictionary<string, string> UpdateCommissionStatus(string comissionid, string status)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "Status=" + status);
            response.Add("table", "T_Commision");
            response.Add("where", "CommisionId=" + comissionid);
            response.Add("database", "seeinsured");
            return response;
        }
        public static Dictionary<string, string> UpdateCommissionDetail(string basicPer, string basicTDS, string incentivePer, string incentiveTDS, int comissionid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "BasicPer='" + basicPer + "',BasicTDS='" + basicTDS + "',IncentivePer='" + incentivePer + "',IncentiveTDS='" + incentiveTDS + "'");
            response.Add("table", "T_Commision");
            response.Add("where", "CommisionId=" + comissionid);
            response.Add("database", "seeinsured");
            return response;
        }
    }
}
