﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostUtility.Admin_Backend
{
    public static class AdminBackend_AgencyHelper
    {
        private static Dictionary<string, string> response { get; set; }
        public static Dictionary<string, string> GetAgencyList(string agencyId = "", string status = "", string agencyName = "")
        {
            response = new Dictionary<string, string>();
            string whereCondition = string.Empty;
            if (!string.IsNullOrEmpty(status))
            {
                whereCondition = "Status=" + status;
            }

            if (!string.IsNullOrEmpty(agencyId))
            {
                whereCondition += !string.IsNullOrEmpty(whereCondition) ? " and AgencyID=" + agencyId : "AgencyID=" + agencyId;
            }

            if (!string.IsNullOrEmpty(agencyName))
            {
                whereCondition += !string.IsNullOrEmpty(whereCondition) ? " and AgencyName='" + agencyName + "'" : "AgencyName='" + agencyName + "'";
            }

            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_Agency");
            response.Add("where", whereCondition);
            response.Add("database", "seeinsured");
            return response;
        }
    }
}
