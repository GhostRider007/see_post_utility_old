﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostUtility.Admin_Backend
{
    public static class AdminBackend_AccountHelper
    {
        private static Dictionary<string, string> response { get; set; }
        public static Dictionary<string, string> GetLoginMemberByUserId(string userId, string password = "")
        {
            response = new Dictionary<string, string>();
            string wherecon = "UserId='" + userId + "'" + (!string.IsNullOrEmpty(password) ? " and Password='" + password + "'" : string.Empty);
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_MemberLogin");
            response.Add("where", wherecon);
            response.Add("database", "seeinsured");
            return response;
        }
        public static Dictionary<string, string> IsUserLoginSuccess(int loginId)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_MemberLogin");
            response.Add("where", "LoginId=" + loginId);
            response.Add("database", "seeinsured");
            return response;
        }
        public static Dictionary<string, string> IsUserAlreadyExist(string userId)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_MemberLogin");
            response.Add("where", "UserId=" + userId);
            response.Add("database", "seeinsured");
            return response;
        }
        public static Dictionary<string, string> UpdateMemberImage(string loginId, string imgurl)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "Image='" + imgurl + "'");
            response.Add("table", "T_MemberLogin");
            response.Add("where", "LoginId=" + loginId);
            response.Add("database", "seeinsured");
            return response;
        }
        public static Dictionary<string, string> GetLoginMemberByLoginId(string loginId, string password = "")
        {
            response = new Dictionary<string, string>();
            string wherecon = "LoginId='" + loginId + "'" + (!string.IsNullOrEmpty(password) ? " and Password='" + password + "'" : string.Empty);
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "T_MemberLogin");
            response.Add("where", wherecon);
            response.Add("database", "seeinsured");
            return response;
        }
        public static Dictionary<string, string> Update_MemberPassword(string password, string loginId)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "Password='" + password + "',UpdatedDate=getdate()");
            response.Add("table", "T_MemberLogin");
            response.Add("where", "LoginId='" + loginId + "'");
            response.Add("database", "seeinsured");
            return response;
        }
        public static Dictionary<string, string> Admin_ChangeAgencyPassword(string password, string agencyId)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "AgencyId='" + agencyId + "'");
            response.Add("table", "aspnet_user");
            response.Add("where", "Password='" + password + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetHealth_BookingDetail(string fromDate = "", string toDate = "", string agencyId = "", string insuranceType = "", string insurer = "", string product = "")
        {
            string tablename = "T_NewEnquiry_Health a inner Join T_Health_Proposal b on a.enquiry_id = b.enquiryid  inner join T_Health_Payment c on a.enquiry_id = c.enquiryid inner join T_Enquiry_Response d on a.enquiry_id = d.enquiry_id";

            string fileds = "a.AgencyId as AgencyID, a.AgencyName as AgencyName, a.Enquiry_Id as Enquiry_Id , 'Health' as Insurance,";
            fileds = fileds + "(Select CompanyName from tbl_Company where CompanyId = b.companyid) as Insurer,";
            fileds = fileds + "b.pay_policy_Number as PolicyNo, d.suminsured as SumInsured, CONCAT(b.title,' ',b.firstname,' ',b.lastname) as Proposer_Name,";
            fileds = fileds + "(select top 1 CONCAT(title,' ',firstname,' ',lastname) from T_Health_Insured where enquiryid = a.enquiry_id) as Insured_Name,";
            fileds = fileds + "c.pp_premium as Basic ,c.pp_serviceTax as GST ,c.pp_totalPremium as Premium, 0 as Discount,0 as TDS, ";
            fileds = fileds + "(select JSON_VALUE(perposalrequest,'$.products.policyName') from T_Enquiry_Response where enquiry_id = a.enquiry_id) as ProductName,";
            fileds = fileds + "a.Policy_Type as PolicyType,b.periodfor as Tenure, c.CreatedDate as PolicyDate";

            string whereCondition = "(b.pay_policy_Number is not null and b.pay_policy_Number <> '')";

            if (!string.IsNullOrEmpty(agencyId))
            {
                whereCondition = whereCondition + " and a.AgencyId='" + agencyId + "'";
            }

            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                whereCondition = whereCondition + " and  c.CreatedDate > (select CAST(CAST(GETDATE() AS DATE) AS DATETIME))";
            }
            else
            {
                if (!string.IsNullOrEmpty(fromDate))
                {
                    whereCondition = whereCondition + " and  c.CreatedDate > '" + fromDate + "'";
                }

                if (!string.IsNullOrEmpty(toDate))
                {
                    whereCondition = whereCondition + " and  c.CreatedDate <= DATEADD(Day, 1, '" + toDate + "')";
                }
            }

            if (!string.IsNullOrEmpty(insurer) && insuranceType.ToLower() == "health")
            {
                whereCondition = whereCondition + " and ((Select CompanyName from tbl_Company where CompanyId = b.companyid) = '" + insurer + "' )";
            }

            if (!string.IsNullOrEmpty(product) && insuranceType.ToLower() == "health")
            {
                whereCondition = whereCondition + " and ((select JSON_VALUE(perposalrequest,'$.products.policyName') from T_Enquiry_Response where ISJSON(perposalrequest) = 1 and perposalrequest is not null and enquiry_id = a.enquiry_id) = '" + product + "' )";

            }

            whereCondition = whereCondition + " order by c.CreatedDate desc";

            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetMotor_BookingDetail(string fromDate = "", string toDate = "", string agencyId = "", string insuranceType = "", string insurer = "", string product = "")
        {
            string tablename = "T_NewEnquiry_Motor a inner join T_Motor_Proposal b on a.enquiry_id = b.enquiry_id inner join T_Motor_Enquiry_Response c on a.enquiry_id = c.enquiry_id";

            string fileds = "a.AgencyId as AgencyID, a.AgencyName as AgencyName,a.Enquiry_Id as Enquiry_Id, a.vehicletype as Insurance,";
            fileds = fileds + "(select top 1 CompanyName from tblMaster_ProductDetails where CompanyId = c.chainid ) as Insurer,";
            fileds = fileds + "c.policynumber as PolicyNo, (select JSON_VALUE(proposalresponse,'$.vehicle.vehicleIDV.idv') from T_Motor_Enquiry_Response where enquiry_id = a.enquiry_id) as SumInsured,";
            fileds = fileds + "CONCAT(b.title,' ',b.firstname,' ',b.lastname) as Proposer_Name, CONCAT(a.BrandName,' ',a.ModelName,' ',a.VarientName) as Insured_Name,";
            fileds = fileds + "(select JSON_VALUE(proposalresponse,'$.ProductDetails.netPremium') from T_Motor_Enquiry_Response where enquiry_id = a.enquiry_id) as Basic,";
            fileds = fileds + "(select JSON_VALUE(proposalresponse,'$.ProductDetails.totalTax') from T_Motor_Enquiry_Response where enquiry_id = a.enquiry_id) as GST,";
            fileds = fileds + "(select JSON_VALUE(proposalresponse,'$.ProductDetails.grossPremium') from T_Motor_Enquiry_Response where enquiry_id = a.enquiry_id) as Premium,";
            fileds = fileds + "0 as Discount,0 as TDS, (select JSON_VALUE(proposalresponse,'$.ProductName') from T_Motor_Enquiry_Response where enquiry_id = a.enquiry_id) as ProductName,";
            fileds = fileds + "b.policyholdertype as PolicyType, '---' as Tenure,b.createddate as PolicyDate";

            string whereCondition = "(c.policynumber is not null and c.policynumber <> '')";

            if (!string.IsNullOrEmpty(agencyId))
            {
                whereCondition = whereCondition + " and a.AgencyId='" + agencyId + "'";
            }

            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                whereCondition = whereCondition + " and  c.CreatedDate > (select CAST(CAST(GETDATE() AS DATE) AS DATETIME))";
            }
            else
            {
                if (!string.IsNullOrEmpty(fromDate))
                {
                    whereCondition = whereCondition + " and  c.CreatedDate > '" + fromDate + "'";
                }

                if (!string.IsNullOrEmpty(toDate))
                {
                    whereCondition = whereCondition + " and  c.CreatedDate <= DATEADD(Day, 1, '" + toDate + "')";
                }
            }
            if (!string.IsNullOrEmpty(insuranceType) && insuranceType.ToLower() == "motor")
            {
                whereCondition = whereCondition + " and (a.vehicletype='Private_Car' or a.vehicletype='Bike')";
            }
            if (!string.IsNullOrEmpty(insurer) && insuranceType.ToLower() == "motor")
            {
                whereCondition = whereCondition + " and ((select top 1 CompanyName from tblMaster_ProductDetails where CompanyId = c.chainid ) = '" + insurer + "')";
            }

            if (!string.IsNullOrEmpty(product) && insuranceType.ToLower() == "motor")
            {
                whereCondition = whereCondition + " and (select JSON_VALUE(proposalresponse,'$.ProductName') from T_Motor_Enquiry_Response where ISJSON(proposalresponse) = 1 and proposalresponse is not null and enquiry_id = a.enquiry_id) = '" + product + "')";
            }

            whereCondition = whereCondition + " order by c.CreatedDate desc";

            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "motor");
            return response;
        }
        public static Dictionary<string, string> GetAgentDetail()
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "AgencyId, AgencyName");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "aspnet_user");
            response.Add("where", "AgencyName is NOT NULL and IsApproved=1");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetCompanyDetail(string insuranceType)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("fieldvalue", string.Empty);
            if (insuranceType.ToLower() == "health")
            {
                response.Add("column", "CompanyId as CompanyId, CompanyName as CompanyName,logoUrl as ImgUrl");
                response.Add("table", "tbl_Company");
                response.Add("database", "api");
            }
            else
            {
                response.Add("column", "CompanyId as CompanyId, Supplier as CompanyName, Image as ImgUrl");
                response.Add("table", "T_MotorCredential");
                response.Add("database", "motor");
            }
            response.Add("where", string.Empty);
            return response;
        }
        public static Dictionary<string, string> GetCompanyProductDetail(string insuranceType, string insuranceId)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("fieldvalue", string.Empty);
            if (insuranceType.ToLower() == "health")
            {
                response.Add("column", "ProductName as ProductName,ProductId as ProductId");
                response.Add("table", "tbl_Product");
                response.Add("where", "CompanyId = " + insuranceId + " and Active = 1");
                response.Add("database", "api");
            }
            else
            {
                response.Add("column", "Distinct ProductName as ProductName");
                response.Add("table", "tblMaster_ProductDetails");
                response.Add("where", "CompanyId = " + insuranceId);
                response.Add("database", "motor");
            }
            return response;
        }
    }
}
