﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostUtility.FrontSection
{
    public static class Administration_Helper
    {
        private static Dictionary<string, string> response { get; set; }
        public static Dictionary<string, string> GetAgentDetailByUserId(string userid)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "aspnet_user");
            response.Add("where", "IsApproved=1 and UserName='" + userid + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> GetAgentDetailByAgencyId(string agencyId)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", "*");
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "aspnet_user");
            response.Add("where", "AgencyId='" + agencyId + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> Update_AgencyPassword(string password, string agencyId)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "Password='" + password + "',UpdatedDate=getdate()");
            response.Add("table", "aspnet_user");
            response.Add("where", "AgencyId='" + agencyId + "'");
            response.Add("database", "api");
            return response;
        }
        public static Dictionary<string, string> UpdateLastLoginDate(string userId)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "update");
            response.Add("column", string.Empty);
            response.Add("fieldvalue", "LastLoginDate=getdate()");
            response.Add("table", "T_Agency");
            response.Add("where", "UserId=" + userId);
            response.Add("database", "seeinsured");
            return response;
        }
        public static Dictionary<string, string> Insert_LeadRequest(string agencyName, string agencyId, string name, string email, string contactNo, string age, string gender, string service, string remark)
        {
            response = new Dictionary<string, string>();
            response.Add("type", "insert");
            response.Add("column", "AgencyName,AgencyID,Name,Email,ContactNo ,Age ,Gender ,Service ,Remark");
            response.Add("fieldvalue", "'" + agencyName + "','" + agencyId + "','" + name + "','" + email + "','" + contactNo + "','" + age + "','" + gender + "','" + service + "','" + remark + "'");
            response.Add("table", "T_LeadRequest");
            response.Add("where", string.Empty);
            response.Add("database", "seeinsured");
            return response;
        }
        public static Dictionary<string, string> GetEnquiryDetailsByAgencyPro(string agencyid)
        {
            response = new Dictionary<string, string>();

            string fileds = "a.Id,a.AgencyName, a.CreatedDate,a.enquiry_id,a.ModelName,a.VarientName,a.vehicletype,b.proposalnumber as ProposalNo,b.chainid as ChainId,b.suminsured as PremimumAmount,c.firstname as FirstName, c.lastname as lastName ,b.policynumber,b.policypdflink";
            string tablename = "T_NewEnquiry_Motor a inner Join T_Motor_Enquiry_Response b on a.enquiry_id = b.enquiry_id  inner Join T_Motor_Proposal c on a.enquiry_id = c.enquiry_id";
            string whereCondition = " a.AgencyId='" + agencyid + "' and b.transactionnumber is  null and b.proposalnumber is not Null and b.paymentstatus is null  order by createddate desc";

            response.Add("type", "get");
            response.Add("column", fileds);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", tablename);
            response.Add("where", whereCondition);
            response.Add("database", "motor");
            return response;
        }
    }
}
