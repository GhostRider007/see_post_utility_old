﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostUtility.Common
{
    public static class AutoCompleteHelper
    {
        private static Dictionary<string, string> response { get; set; }
        public static Dictionary<string, string> GetAgentDetail_Like(string agencyName = "", string agencyId = "", string status = "", string columns = "")
        {
            string fields = !string.IsNullOrEmpty(columns) ? columns : "*";
            string wherecon = string.Empty;
            if (!string.IsNullOrEmpty(agencyName.Trim()))
            {
                wherecon = !string.IsNullOrEmpty(wherecon) ? (wherecon + " and AgencyName like '%" + agencyName + "%'") : " AgencyName like '%" + agencyName + "%'";
            }
            if (!string.IsNullOrEmpty(agencyId.Trim()))
            {
                wherecon = !string.IsNullOrEmpty(wherecon) ? (wherecon + " and AgencyId=" + agencyId) : " AgencyId=" + agencyId;
            }
            if (!string.IsNullOrEmpty(status.Trim()))
            {
                wherecon = !string.IsNullOrEmpty(wherecon) ? (wherecon + " and status='" + status + "'") : " status='" + status + "'";
            }

            response = new Dictionary<string, string>();
            response.Add("type", "get");
            response.Add("column", fields);
            response.Add("fieldvalue", string.Empty);
            response.Add("table", "aspnet_user");
            response.Add("where", wherecon);
            response.Add("database", "api");
            return response;
        }
    }
}
